var gulp            = require('gulp');
var browserSync     = require('browser-sync');
var config          = require('./gulp.config.js')();
var sourcempas      = require('gulp-sourcemaps');
var sass            = require('gulp-sass');
var autoprefixer    = require('gulp-autoprefixer');
var util            = require('gulp-util');

/*DEFAULT TASKS*/
gulp.task('default',['serve','sass:watch']);
gulp.task('sass',function () {
    return gulp
        .src(config.sass.src)
        .pipe(sourcempas.init())
        .pipe(sass(
            {
                outputStyle:config.sass.style
            }
        ).on('error',sass.logError))
        .pipe(sourcempas.write())
        .pipe(autoprefixer())
        .pipe(gulp.dest(config.src))
});

/*BUILD TASKS*/

gulp.task('serve', function() {

    browserSync({
        files: [config.src +'/**/*.{css,js,html,php}'],
        proxy: config.bsConfig.proxy,
        notify:true,
        ghostMode: {
            clicks: true,
            forms: true,
            scroll: true
        },
        logLevel: "debug",
        logFileChanges: true,
        reloadDelay: 1000
    });

});


gulp.task('sass:watch', function() {
    gulp.watch(config.sass.src,['sass'])
});



function log(msg){
    if(typeof(msg) === 'object'){
        for (var item in msg){
            if (msg.hasOwnProperty(item)){
                util.log(util.colors.blue(msg[item]))
            }
        }
    } else {
        util.log(util.colors.blue(msg));
    }
}

