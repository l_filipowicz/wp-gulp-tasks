module.exports = function(){
    var project_name = "/trans-auto";
    var src = __dirname + '/wp-content/themes' + project_name;

    var config = {
        src     : src,
        css     : src + '/css',
        images  : src + '/img',
        scripts : src + '/js',
        sass    : {
            src  :src + '/sass/**/*.scss',
            style:''

        },
        bsConfig: {
            defaultPort: '80',
            proxy:'trans-auto.dev'
        }
    };
    return config;
};


